:warning: **IMPORTANT : this repository has been moved to GitHub, as it has a better JupyterNotebook support. So this version is not updated, see https://github.com/tlunet/pint-workshops for the actual version !** :warning: 

# FSMP Workshop : Time Parallel Time Integration

Webpage : https://sciencesmaths-paris.fr/en/e/news-en/time-parallel-time-integration-1

## General Details

Each course session starts with two lecture sessions (9h->10h, 10h15->11h15), and is then followed by a practical exercise session (11h30 -> 12h).

The exercise sessions consist on small demos with Python code and Jupyter Notebook, containing additional tasks that can be completed during the week. Don't hesitate to open a [Gitlab Issue](https://gitlab.com/tlunet/pint-workshops/-/issues) to put there your solution for checking, or if you see any problem in the current given code (need a Gitlab account for that).

Several (free and open source) tools are used during the sessions :

- Python : you can install it on your own computed using [Anaconda](https://www.anaconda.com/download), which comes with many library ([Matplotlib](https://matplotlib.org/), [Numpy](https://numpy.org/), ...) and even some Interactive Development Environment (IDE) like [Spyder](https://www.spyder-ide.org/).
- VSCode : a multi-language editor, that provide nice integration for Jupyter Notebooks, that you can [install on any system ...](https://code.visualstudio.com/download)


## Code for Exercise Sessions

1. [Tuesday June 20](./s1/)



